
#=====================================================================[ header ]

from util.freezeable  import *        # utilities and hippo modules
from util.virtualMaze import *
from util.percepts    import *
from util.sampler     import *

import matplotlib                     # plotting (non-interactive 'Agg' backend)
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from PIL import Image 				  # image (manual plots)
from PIL import ImageDraw

import cPickle as pickle

#----------------------------------------------------------------------[ setup ]

# spatial data container
class Data( Freezeable ):
	def __init__( self ):
		# navigation data
		self.pos = None      # position data (trajectory)
		self.dir = None      # direction per time step
		self.grd = None      # grid population activity
		self.vis = None      # visual data (SFA features) (neg range flipped; features scaled to [0;1])
		self.vis_raw = None  # raw visual data (features extracted by SFA)
		# sets
		self.nav_prc = []    # nav data as list of percepts per time step
		# shortcuts
		self.rec     = None  # previously generated cluster activity
		self.rec_raw = None  # as above w/o post-processing (exp scaling)
		self.network = None  # previously trained network
		self.freeze()

data    = Data()
network = PerceptNetwork()

#------------------------------------------------------------[ data processing ]

def recordClusterResponse( percepts, anim=False ):

	# record cluster activity from within the network
	cluster_activity = []
	for t, prc in enumerate( percepts ):

		cluster_activity.append( network.getClusterActivity( prc ) ) # activity vectors grow dynamically as the network adds new percept clusters
		
		# show progress bar
		###done = int(((t+1)*50.0)/len(percepts))
		###sys.stdout.write('\r[RUNNING]['+'='*done+'-'*(50-done)+']~[%d/%.2f'%((t+1),float(t+1)/len(percepts)*100.0)+'%]')
		###sys.stdout.flush()
		
		# draw animation frames (optional)
		# if anim: plotNetwork( anim_frame=t ) ######## reinstate

		################################## REMOVE
		#if t <= 500 and anim:
		#	plotNetwork( anim_frame=t )
		#if t >  500 and anim:
		#	plotNetwork( anim_frame=t, sequence=network.next( int(5+np.random.rand()*5) ) )
		################################## REMOVE

	print '\n'

	# store activity in (static) numpy array
	data_raw = np.zeros( [ len(cluster_activity), len(cluster_activity[-1]) ] )
	data_raw[:] = np.NAN # mark the activity of all clusters as NaN until they become active

	for i in range( len(cluster_activity) ):
		data_raw[ i, 0:len(cluster_activity[i]) ] = cluster_activity[i]

	# prepare post-processing of cluster activity
	data           = data_raw.copy()
	nan_mask       = np.isnan( data_raw )
	data[nan_mask] = 0.0

	# post-processing, step 1: normalization
	for i in range( len(data) ):
		data[i] = np.exp(data[i]) / np.sum(np.exp(data[i]))

	# post-processing, step 2: k-winner take all
	"""
	for i in range( len(data) ):
		k_winner_mask = data[i] < np.sort(data[i])[-10] # k = 10
		data[i][k_winner_mask] = 0.0
	"""

	# restore NAN markers once post-processiong is done
	data[nan_mask] = np.NAN

	return data, data_raw

def plotClusterActivity( pos_data, rec_data, bins=80, jet_scale=False ):
	# copy data & mask NaN markers
	rec_data_m = rec_data.copy()
	nan_mask   = np.isnan( rec_data )
	rec_data_m[nan_mask] = 0.0
	# plot cluster activity using numpy & matplotlib
	for i in range( rec_data_m.shape[1] ):
		plt.clf()
		plt.figure().set_size_inches(3,3) # works in tandem w/ dpi parameter of savefig()
		plt.hexbin( pos_data[:,0], pos_data[:,1], C=rec_data[:,i] )
		plt.gca().invert_yaxis()
		plt.savefig( 'plots/cluster_'+str(i).zfill(3)+' ('+str(np.nanmin(rec_data_m[:,i]))+';'+str(np.nanmax(rec_data_m[:,i]))+').png', dpi=300 )
		plt.close()

def plotNetwork( anim_frame=None, sequence=None ):

	# setup
	r     = 100
	path  = data.pos
	size  = 800
	scale = size/(r*2)
	img   = Image.new( 'RGB', (size,size), 'white' )
	pix   = img.load()
	drw   = ImageDraw.Draw( img )

	# draw trajectory
	for i in range( len(path)-1 if anim_frame==None else anim_frame-1  ):
		drw.line( (path[i][0]*scale+400,path[i][1]*scale+400,path[i+1][0]*scale+400,path[i+1][1]*scale+400), width=1, fill='black' )

	# draw cluster percept extent
	for c_id in network.clusters:
		c = network.clusters[c_id]
		for prc in c.percept_list:
			drw.line( ( c.anchor[0]*scale+400,c.anchor[1]*scale+400, prc._pos[0]*scale+400,prc._pos[1]*scale+400), width=1, fill='blue' )

	# draw cluster links
	cmap = matplotlib.cm.get_cmap( 'winter' )
	link_max = max( [1] or [ link_dict[max( [1] or link_dict,key=link_dict.get)] for link_dict in [network.clusters[c_id].links for c_id in network.clusters] ] )
	for c_id_1 in network.clusters:
		c_1 = network.clusters[c_id_1]
		for c_id_2 in c_1.links:
			c_2 = network.clusters[c_id_2]
			# -- find relative link strength
			link_strength  = c_1.links[c_id_2] if c_id_1 not in c_2.links else max(c_1.links[c_id_2],c_2.links[c_id_1])
			link_strength /= float(link_max)
			color = np.array( cmap(link_strength)[:3] )*255.0
			# -- draw the link
			drw.line( ( c_1.anchor[0]*scale+400,c_1.anchor[1]*scale+400, c_2.anchor[0]*scale+400,c_2.anchor[1]*scale+400), width=1, fill=tuple(np.array(color,dtype=np.int32)) )

	# draw cluster centers
	for c_id in network.clusters:
		c = network.clusters[c_id]
		drw.ellipse( ( c.anchor[0]*scale+400-5,c.anchor[1]*scale+400-5, c.anchor[0]*scale+400+5, c.anchor[1]*scale+400+5), fill=(0,150,200) )

	# mark last active cluster center
	"""
	if network.last:
		c = network.last
		for i in range(10,0,-1):
			drw.ellipse( ( c.anchor[0]*scale+400-i,c.anchor[1]*scale+400-i, c.anchor[0]*scale+400+i, c.anchor[1]*scale+400+i), fill=(205-10*i,0,0) )
	"""

	# mark sequence (optional)
	if sequence != None:
		for c in sequence:
			for i in range(6,0,-1):
				drw.ellipse( ( c.anchor[0]*scale+400-i,c.anchor[1]*scale+400-i, c.anchor[0]*scale+400+i, c.anchor[1]*scale+400+i), fill=(205-10*i,0,205-10*i) )
			
	# write image to disk
	if anim_frame != None:
		img.save( 'animation/frame_'+str(anim_frame).zfill(4)+'.jpg' )
	else:
		img.save( 'network_state ('+str(len(path))+').png' )

#-----------------------------------------------------------------------[ main ]

def main():

	# generate new visual data batch (optional)
	if 'new_data' in sys.argv:
		print 'Generating new data batch (%d steps)'% ( int(sys.argv[2]) )
		vm = VirtualMaze()
		vm.generateNavData( int(sys.argv[2]) )
		sys.exit()

	# load navigation data
	try:
		print 'Reading data from disk..',
		step_limit =  int(sys.argv[ sys.argv.index('limit')+1 ]) if 'limit' in sys.argv else None
		data.vis = np.loadtxt('data/dat_vis.txt')[:step_limit] # visual input along trajectory
		data.grd = np.loadtxt('data/dat_grd.txt')[:step_limit] # grid cell activity along trajectory
		data.pos = np.loadtxt('data/dat_pos.txt')[:step_limit] # position over time  (visualization & debugging only)
		data.dir = np.loadtxt('data/dat_dir.txt')[:step_limit] # direction over time (visualization & debugging only)
		step_limit = len(data.vis) # if no limit was provided
	except IOError as err:
		print 'fail.\nNav data not found.'
		sys.exit()
	print 'done.\n', step_limit, 'time steps loaded.'

	# put data into percepts
	for i in range( step_limit ):
		vis_prc = Percept( 'visual', data.vis[i], data.pos[i], data.dir[i], i )
		grd_prc = Percept( 'grid',   data.grd[i], data.pos[i], data.dir[i], i )
		data.nav_prc.append( [vis_prc, grd_prc] )

	# load network (optional)
	if 'load_network' in sys.argv:
		global network
		network_file = open( 'data/network', 'rb' )
		network = pickle.load( network_file )

	# create (and store) cluster activity (only if none was loaded)
	if 'new_recording' in sys.argv:
		data.rec, data.rec_raw = recordClusterResponse( data.nav_prc, anim=('animation' in sys.argv) )
		np.savetxt( './data/dat_rec.txt', data.rec )
		np.savetxt( './data/dat_rec (raw).txt', data.rec_raw )
	else:
		try:
			data.rec     = np.loadtxt('data/dat_rec.txt')[:step_limit]
			data.rec_raw = np.loadtxt('data/dat_rec (raw).txt')[:step_limit]
		except IOError:
			print 'Warning! No data recording is generated or loaded (file not found).'

	# sample SFA network within the virtual maze
	if 'sfa_sample' in sys.argv:
		vm = VirtualMaze()
		sampler = Sampler( vm.limits, vm.getRawSampleAtPosition )
		sampler.execute()
		sys.exit()

	# full sample of cluster responses over the complete environment of the virtual maze
	if 'network_sample' in sys.argv:
		vm = VirtualMaze()

		def samplerFunc_FIX( pos, dir ):
			# acquire sfa scaling values from the raw visual data
			if 'scale_val' not in samplerFunc_FIX.__dict__:
				raw_abs = np.abs( np.loadtxt( './data/dat_vis (raw).txt' ) )
				samplerFunc_FIX.scale_val = np.array( [ 1.0/np.max(sig) for sig in raw_abs.T ] )
			# put together percept data
			vis = np.abs( vm.getRawSampleAtPosition( pos, dir ) ) * samplerFunc_FIX.scale_val
			grd = vm.getGridActivity( pos )
			vis_prc = Percept( 'visual', vis, (0,0), (0,0), 0 )
			grd_prc = Percept( 'grid',   grd, (0,0), (0,0), 0 )
			# get network response
			if np.isnan( vis[0,0] ):
				return np.empty( len(network.clusters) ) * np.NAN
			else:
				return network.getClusterActivity_NoLearn( [ vis_prc, grd_prc ] )

		sampler = Sampler( vm.limits, samplerFunc_FIX, sample_signals=len(network.clusters) )
		sampler.execute();
		sys.exit()
	
	# plot cluster activity (place fields) & network state (trajectory & cluster links)
	if 'cluster_plot' in sys.argv:
		plotClusterActivity( data.pos, data.rec, bins=80 )
		plotNetwork()

	# print some cluster info
	if 'cluster_info' in sys.argv:
		for i,c_id in enumerate(network.clusters):
			network.clusters[c_id].printCluster(i)
			print ''

	# save network to disk once all is said and done
	if 'save_network' in sys.argv:
		pickle.dump( network, open( 'data/network (' + step_limit + ')', 'wb' ) )
		print 'Network saved.'
	
	print 'all done'

main()

""" NOTE: DATA HANDLING

OUT: virtualMaze.generateNavData()
	-> creates raw visual data: "dat_vis (raw).txt"
	-> absolutes() and norms data to [0,1]: "dat_vis.txt"
	   . scaling values to norm additional data later on can be computed from raw data
	   . no polarization heuristic is applied

OUT: virtualMaze.getRawSampleAtPosition()
	-> returns raw SFA response only (no abs() applied and no scaling to [0,1]-norm)

IN: plotClusterActivity(), aka default / "natural" plot
	-> feed network with abs()/normed data from "dat_vis.txt"
	-> apply softmax() & plot cluster responses

IN: sampler.execute()
	-> sampling function gets raw visuals and applies abs() & scales to norm
	-> visual and grid data is wrapped into a percept and used to query network
	-> network sampled over full environment & multiple directions

!! THESE FOUR FUNCTIONS NEED TO BE COHERENT AND APPLY ANY POST-PROCESSING EQUALLY !!
"""

"""
Things to try:
- Scale visual data to ]-1,1] instead of [0,1], i.e., don't apply abs()
- Weigh grid components so that larger grids become more impactful
"""
