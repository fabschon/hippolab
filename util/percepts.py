
#=====================================================================[ header ]

import numpy as np

from util.freezeable        import *
from util.levenshtein       import *
from scipy.spatial.distance import *

# set default distance functions
DST_WRD = levenshtein
DST_GRD = euclidean    # options: http://docs.scipy.org/doc/scipy/reference/spatial.distance.html
DST_VIS = euclidean    # cosine alternative: lambda x,y: cosine(x,y)

# threshold values: if DST_xxx(x,y) is..
DST_EQL_VIS = 0.1  # ..smaller than DST_EQL, items are considered to be equal
DST_DFF_VIS = 0.6  # ..larger  than DST_DFF, items are considered to be different enough for storage
DST_EQL_GRD = 0.5
DST_DFF_GRD = 4.0
DST_EQL_WRD = 0
DST_DFF_WRD = 1

def generateID():
	generateID.i = 0 if 'i' not in generateID.__dict__ else generateID.i+1
	return generateID.i

#--------------------------------------------------------------------[ percept ]

class Percept(Freezeable):

	def __init__( self, percept_type, percept_data, _pos=None, _dir=None, _t=None ):
		self.type = percept_type
		self.data = percept_data
		self._pos = _pos  # position   \
		self._dir = _dir  # direction   > ground truth for visualization & analysis
		self._t   = _t    # time step  /
		self.__config__()
		self.freeze()

	def __config__( self ):
		if self.type == 'visual':
			self.dst_fnc = DST_VIS
			self.dst_eql = DST_EQL_VIS
			self.dst_dff = DST_DFF_VIS
		elif self.type == 'grid':
			self.dst_fnc = DST_GRD
			self.dst_eql = DST_EQL_GRD
			self.dst_dff = DST_DFF_GRD
		elif self.type == 'word':
			self.dst_fnc = DST_WRD
			self.dst_eql = DST_EQL_WRD
			self.dst_dff = DST_DFF_WRD

	def equals( self, percept ):
		return self.type == percept.type and self.dst_fnc( self.data, percept.data ) <= self.dst_eql

	def differs( self, percept ):
		return self.type != percept.type or self.dst_fnc( self.data, percept.data ) >= self.dst_dff

	# measure of DIFFERENCE: the more the offered percept differs, the higher the response value will be in [0,max]
	def difference( self, percept_list ):
		"""
		r = np.zeros( len(percept_list) )
		for i,p in enumerate( percept_list ):
			r[i] = np.nan if self.type != p.type else dst_fnc( self.data, p.data )
		"""
		return np.array( [np.nan if self.type != p.type else self.dst_fnc( self.data, p.data ) for p in percept_list] )

	# measure of SIMILARITY: the more similar the percepts, the higher the response value will be in [0,1]
	def similarity( self, percept_list ):
		"""                                        0    DST_EQL         DST_DFF
		Scale for percept responses (difference):  |-------|---------------|------>
		For the similarity measure, the difference measure gets capped at 
		dst_dff, reversed, and mapped to [0,1]. Responses with difference score
		greater/equal to dst_dff are be mapped to similarity zero; difference
		scores below dst_dff are mapped to a value in [0,1]
		NAN values fail every comparison and are thus first mapped to dst_dff
		and thus to similarity zero.
		"""
		sim_scores = [ dff if dff<self.dst_dff else self.dst_dff for dff in self.difference(percept_list) ]
		sim_scores = [ 1.0-(dff/self.dst_dff) for dff in sim_scores ]
		return np.array( sim_scores )

#--------------------------------------------------------------------[ cluster ]

class Cluster(Freezeable):

	def __init__( self, percepts ):
		self.percept_list = percepts    # list of percepts associated in this cluster
		self.anchor = percepts[0]._pos  # position where cluster was started
		self.id     = generateID()      # unique cluster id
		self.links  = {}                # link strength to other clusters (id as key, strength as value)
		self.freeze()

	# measure of FAMILIARITY: the more familiar the percept is, the higher the response value will be
	def __response__( self, percept_list ):
		"""
		Response of a single cluster to a given percept_list (snapshot 
		containing one percept per input modality) is computed by looking
		for the most similar cluster-percept to each of the currently
		offered new percepts. Those most-similar scores are added up and
		divided by the no. of modalities, i.e., len(percept_list).
		If all percepts are already known, the DIFFERENCE of each one will be
		close to 0.0 for AT LEAST ONE stored percept (and, consequently, the
		SIMILARITY will be close to 1.0).
		The final response of the cluster is the average over the familiarty
		scores of those cluster percepts which are most familiar with the 
		new percept of their own modality.
		"""
		r = np.array( [np.nanmax(p.similarity(self.percept_list)) for p in percept_list] )
		return np.sum(r)/len(percept_list)

	def present( self, percept_list ):
		"""
		Response of a cluster to any percept. The cluster will respond with how
		familiar the percepts are AND what (if anything) it did with the 
		percepts and/or how the percepts should be handled by the overall 
		network.
		Ideally only the familiarity measure would be returned and the network
		would learn at what thresholds of familiarity a cluster indicates 
		knowledge of a set of percepts. Since we don't have these threshold 
		values, however, we shall return these discreet response values to 
		indicate that information.
		Values: 0 (recommend seed), 1 (percepts [now] known), -1 (don't care)
		"""
		known  = [ self.knows(p) for p in percept_list ]
		wanted = [ self.wants(p) for p in percept_list ]
		
		# case 01: percept list contains known and unknown percepts
		#          -> claim & request network to associate the new percepts with this cluster
		if any(known) and any(wanted):
			self.associate( percept_list )
			return -1, self.__response__( percept_list )

		# case 02: signal seed recommendation (for all stored percepts, the new percepts differ())
		#          -> this percept will either be claimed by a different cluster, or, if not, will
		#             seed a new cluster by itself
		elif any(wanted):                                 ###-?- ANY or ALL wanted ?
			return  1, self.__response__( percept_list )  ###-?-   -> ANY: 1x idc + 1x new = want
                                                          ###-?-   -> ALL: only n x new = want
		# case 03a: all percepts in the list are known
		elif all(known):
			return -1, self.__response__( percept_list )

		# case 03a: any percept in the list is known (and none are wanted)
		elif any(known):
			return -2, self.__response__( percept_list )

		# case 04: don't care placeholder
		else:
			return  0, self.__response__( percept_list )

	def associate( self, percept_list ):
		wanted = [ self.wants(p) for p in percept_list ]
		self.percept_list.extend( [ p[0] for p in zip(percept_list,wanted) if p[1] ] )

	def knows( self, percept ):
		return any( (percept.equals(p) for p in self.percept_list) )

	def wants( self, percept ):
		return all( (percept.differs(p) for p in self.percept_list) )

	def addLink( self, cluster_id ):
		if cluster_id == self.id: return
		self.links[ cluster_id ] = 1 if cluster_id not in self.links else self.links[ cluster_id ] + 1

	def getLinkedCluster( self, query_id ):
		"""
		Return linked cluster that is neither itself nor the cluster
		who posted the query (and sent its own id as <query_id>).
		"""
		max_link, max_id = 0, -1
		for c_id in self.links:
			if c_id != query_id and self.links[ c_id ] >= max_link:
				max_link    = self.links[ c_id ]
				max_id = c_id
		return max_id

	def printCluster(self, i=-1):
		print '..cluster'+(':' if i==-1 else ('[%d]:'%i)),
		for p in self.percept_list: print p.type,

#------------------------------------------------------------[ percept network ]

class PerceptNetwork(Freezeable):
	
	def __init__( self ):
		self.clusters = {}  # list/network of percept clusters stored in this episode
		self.last = None    # last activated cluster (object, not id)
		self.freeze()

	def __newCluster__( self, percept_list ):
		new_cluster = Cluster( percept_list )
		self.clusters[new_cluster.id] = new_cluster
		self.__newLink__( new_cluster )

	def __newLink__( self, cluster ):
		if self.last: self.last.addLink( cluster.id )
		self.last = cluster

	def getClusterActivity( self, percept_list ):
		
		# edge case: initial cluster during startup / after reset
		if len( self.clusters ) == 0:
			self.__newCluster__( percept_list )
			return np.array([1.0])

		# present input percepts to the current episode network
		interest  = np.zeros( len(self.clusters) )
		recommend = np.zeros( len(self.clusters) )
		link_candidate = (None,0)

		for i, c_id in enumerate( self.clusters ):
			recommend[i], interest[i] = self.clusters[c_id].present( percept_list )

			if recommend[i] < 0 and interest[i] > link_candidate[1]:                        # find nearby clusters
				link_candidate = (c_id,interest[i])                                         # and link them over
		if link_candidate[0] != None: self.__newLink__( self.clusters[link_candidate[0]] )  # time

		# seed new cluster if all existing clusters vote for it
		if np.sum( recommend ) == len( self.clusters ):
			self.__newCluster__( percept_list )

		# return interest level per cluster
		return interest

	def getClusterActivity_NoLearn( self, percept_list ):
		interest  = np.zeros( len(self.clusters) )
		recommend = np.zeros( len(self.clusters) )
		for i, c_id in enumerate( self.clusters ):
			recommend[i], interest[i] = self.clusters[c_id].present( percept_list )
		return interest

	def next( self, n=1 ):
		# start at random cluster
		cnt_cluster = self.clusters[ np.random.choice( self.clusters.keys() ) ]
		last_id = -1
		seq     = []
		# follow links
		for i in range(n):
			seq.append( cnt_cluster )
			next_id = cnt_cluster.getLinkedCluster( last_id )
			if next_id == -1: 
				break			
			else:
				last_id     = cnt_cluster.id
				cnt_cluster = self.clusters[ next_id ]
		# return cluster sequence
		return seq
