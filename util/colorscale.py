#==============================================================[ Color Scaling ]

import numpy

default_scale = 'jet'

def colorScale( value, val_min, val_max ):
	if default_scale   == 'jet':      return colorScale_Jet(value,val_min,val_max)
	elif default_scale == 'hot_cold': return colorScale_HotCold(value,val_min,val_max)

#------------------------------------------------------------------[ Jet Scale ]

def colorScale_Jet( value, val_min, val_max ):
	if val_max <= val_min: return (0,0,0,0)
	# move range from [min,max] to [0,max']
	value   -= val_min
	val_max -= val_min
	# scale value from [0,max'] to [0,4]
	value = (value*4.0)/val_max
	# color array
	rgb = numpy.array([0,0,0])
	# jet scale
	if( value <= 0.5 ):
		rgb[2] = 128 + value*255
	elif( value <= 1.5 ):
		rgb[1] = (value-0.5)*255
		rgb[2] = 255
	elif( value <= 2.5 ):
		rgb[0] = (value-1.5)*255
		rgb[1] = 255
		rgb[2] = 255 - (value-1.5)*255
	elif( value <= 3.5 ):
		rgb[0] = 255
		rgb[1] = 255 - (value-2.5)*255
	else:
		rgb[0] = 255 - (value-3.5)*255
	# return RGBA color tuple
	return (rgb[0],rgb[1],rgb[2],255)

#-------------------------------------------------------------[ Hot-Cold Scale ]

def colorScale_HotCold( value, limit_min, limit_max ):
	# move value and [min;max] range to [0;max'] range
	value     -= limit_min
	limit_max -= limit_min
	# get 1024 resolution scale
	rgb = numpy.array([0,0,0])
	y = int( (value*1024.0)/limit_max )
	# convert to blue-to-red color trace
	if y < 256:
		rgb[1] = y
		rgb[2] = 255
	elif y < 512:
		rgb[1] = 255
		rgb[2] = 512-(y+1)
	elif y < 768:
		rgb[0] = y-512
		rgb[1] = 255
	else:
		rgb[0] = 255
		rgb[1] = 1024-(y+1)
	# return RGBA color tuple