
#=====================================================================[ header ]

import sys
import string
import numpy   as np
import cPickle as pickle

from PIL import Image as img

from OpenGL.GLUT import *
from OpenGL.GLU  import *
from OpenGL.GL   import *

from util.freezeable import *
from util.grid_code  import *

cos  = np.cos
sin  = np.sin
rnd  = np.random.random
asin = np.arcsin

np.random.seed(42)

RAD2DEG = 180.0/np.pi
DEG2RAD = np.pi/180.0

#-----------------------------------------------------------------[ parameters ]

class EmptyOptionContainer( Freezeable ):
	def __init__( self ): pass

class OpenGlParameters( Freezeable ):
	def __init__( self ):
		self.window_width  = 320
		self.window_height =  40
		self.window_title  = 'HippoLab'
		self.aspect_ratio  = float(self.window_width)/float(self.window_height)
		self.clip_near     = 0.5
		self.clip_far      = 1024.0
		self.cam_height    = 3.0
		self.freeze()

opengl_param = OpenGlParameters()

#------------------------------------------------------------[ utility classes ]

class Textures:

	def __init__(self):
		self.index  = {}
		self.skybox = None
		self.floor  = None
		self.wall   = []
		# load available textures
		tex_list = os.listdir('./textures')
		tex_list.sort()
		for f in tex_list:
			s = string.split(f,'.')[0]
			i = self.loadTexture('./textures/'+f)
			self.index[s] = i
			if 'skybox' in s: self.skybox = i
			if 'floor'  in s: self.floor  = i
			if 'wall'   in s: self.wall.append(i)

	def loadTexture(self, filename):
		# open image
		src_img = img.open(filename)
		img_str = src_img.tobytes('raw')
		# create opengl texture object
		img_id = glGenTextures(1)
		glBindTexture(GL_TEXTURE_2D,img_id)
		# texture parameters
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);	
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
		# store texture image (use mipmapping to kill off render artifacts)
		glPixelStorei( GL_UNPACK_ALIGNMENT, 1 )
		glTexImage2D( GL_TEXTURE_2D,    # target
        	          0,                # mipmap level
        	          3,                # color components (3: rgb, 4: rgba)
        	          src_img.size[0],  # texture width
        	          src_img.size[1],  # texture height
        	          0,                # border
        	          GL_RGB,           # format
    	              GL_UNSIGNED_BYTE, # data type
        	          img_str )         # texture data
		# return opengl texture id
		return int(img_id)

class Wall( Freezeable ):

	def __init__( self, left_end, right_end, height, texture=None, offset=5.0 ):
		self.vec_from = left_end
		self.vec_to   = right_end
		self.height   = height
		self.normal   = np.array( [(self.vec_to-self.vec_from)[1],-(self.vec_to-self.vec_from)[0]] )
		self.normal  /= np.sqrt( self.normal[0]**2 + self.normal[1]**2 )
		self.texture  = 0 if texture==None else texture
		self.offset   = 0.0 if offset==None else offset

	# check wether a given point lies in front of the wall
	def facingFront( self, pos ):
		if np.dot( pos-(self.vec_from+0.5*wall_vec), self.normal ) < 0.0: 
			return False
		else:
			return True

	# check wether a given point lies closer to the wall than the allowed offset
	def proximityAlert( self, pos ):
		wall_vec = self.vec_to - self.vec_from
		mu  = (pos[0]-self.vec_from[0])*(self.vec_to[0]-self.vec_from[0]) + (pos[1]-self.vec_from[1])*(self.vec_to[1]-self.vec_from[1])
		mu /= wall_vec[0]**2 + wall_vec[1]**2
		if mu < 0.0:
			return np.sqrt( (pos-self.vec_from)[0]**2 + (pos-self.vec_from)[1]**2 ) < self.offset
		elif mu > 1.0:
			return np.sqrt( (pos-self.vec_to)[0]**2 + (pos-self.vec_to)[1]**2 ) < self.offset
		else:
			proj = self.vec_from + mu*wall_vec
			dist = np.sqrt( (proj-pos)[0]**2 + (proj-pos)[1]**2 )
			return dist < self.offset

	# check wether the path between two positions crosses the wall segment
	def crossedBy( self, pos_old, pos_new ):
		# check 1: old position in front, new position behind wall segment
		wall_vec = self.vec_to - self.vec_from
		if not ( np.dot( pos_old-(self.vec_from+0.5*wall_vec), self.normal ) > 0.0 and \
				 np.dot( pos_new-(self.vec_from+0.5*wall_vec), self.normal ) < 0.0 ):
			return False
		# determine crossing point (moving parallel to wall is cought by check 1)
		xy = self.vec_to-self.vec_from
		uv = pos_new-pos_old
		l = ( pos_old[1] + (self.vec_from[0]*uv[1]-pos_old[0]*uv[1])/uv[0] - self.vec_from[1] ) / ( (1.0-(xy[0]*uv[1])/(uv[0]*xy[1])) * xy[1] )
		m = ( self.vec_from[0] + l*xy[0] - pos_old[0] ) / uv[0]
		# check C: intersection lies between old & new position, and also within wall segment limits
		if l >= 0 and l <= 1.0 and \
           m >= 0 and m <= 1.0:
			return True
		else:
			return False

#---------------------------------------------------------------[ virtual maze ]

def __drawcall__(i):
	glutPostRedisplay()
	glutTimerFunc( 0, __drawcall__, 1 )

class VirtualMaze( Freezeable ):

	def __init__( self, maze_type='circle' ):
		# initialize
		self.opengl_param = opengl_param
		self.__setupOpenGL__()
		# maze elements
		self.walls        = []
		self.textures     = Textures()
		self.limits       = None
		self.display_list = None
		self.__constructMaze__( maze_type )
		self.__createDisplayList__()
		# modalities
		self.sfa_network  = pickle.load( open('full_generic_100k_sim.tsn', 'rb') )
		self.sfa_response = None
		self.grid_cells   = ArtificialGridCode( 32, 5.0, 50.0, 25.0 )  # (no. of grids, min size, max size, max rnd offset)
		# state information
		self.nav_state = np.array( [[np.sum(self.limits[ ::2])/2,   # central x-coord
			                         np.sum(self.limits[1::2])/2],  # central y-coord
			                        [1.0, 0.0]] )                   # arbitrary starting velocity
		self.freeze()

	def __setupOpenGL__(self):
		# window
		glutInit( sys.argv )
		glutInitDisplayMode( GLUT_DOUBLE|GLUT_RGBA|GLUT_DEPTH )
		glutInitWindowSize ( self.opengl_param.window_width, self.opengl_param.window_height )
		glutCreateWindow   ( self.opengl_param.window_title )
		# register rendering functions
		glutDisplayFunc( self.__draw__ )
		glutTimerFunc  ( 0, __drawcall__, 1 )
		# opengl state variables
		glClearColor( 0.0, 0.0, 0.0, 0.0 )  # clear color (background)
		glClearDepth( 1.0 )                 # z buffer
		glEnable   ( GL_DEPTH_TEST )
		glDepthFunc( GL_LEQUAL )
		glCullFace ( GL_BACK )              # backface culling
		glEnable   ( GL_CULL_FACE )
		glEnable   ( GL_TEXTURE_2D )        # texture mapping

	def __constructMaze__( self, maze_type ):
		if maze_type == 'box':
			wall_height = 20.0
			self.walls.append( Wall( np.array([ 0.0, 0.0]), np.array([ 0.0,80.0]), wall_height, self.textures.wall[3] ) )
			self.walls.append( Wall( np.array([ 0.0,80.0]), np.array([80.0,80.0]), wall_height, self.textures.wall[0] ) )
			self.walls.append( Wall( np.array([80.0,80.0]), np.array([80.0, 0.0]), wall_height, self.textures.wall[1] ) )
			self.walls.append( Wall( np.array([80.0, 0.0]), np.array([ 0.0, 0.0]), wall_height, self.textures.wall[2] ) )
			self.limits = np.array([0.0,0.0,80.0,80.0])
		elif maze_type == 'circle':
			height =  30.0
			radius = 100.0
			angle  =   0.0
			step   = 360.0/12
			for w in range(12):
				a = np.array([radius*cos(angle*DEG2RAD),radius*sin(angle*DEG2RAD)])
				b = np.array([radius*cos((angle+step)*DEG2RAD),radius*sin((angle+step)*DEG2RAD)])
				self.walls.append( Wall( b, a, height, self.textures.wall[w/(12/len(self.textures.wall))] ) )
				angle += step
			self.limits = np.array([-radius,-radius,radius,radius])

	def __createDisplayList__(self):
		# sort wall list by texture id
		self.walls= sorted( self.walls, key = lambda wall: wall.texture )
		# start dispaly list
		self.display_list = glGenLists(1)
		glNewList( self.display_list, GL_COMPILE )
		# floor quad
		tex_id = self.textures.floor
		glBindTexture( GL_TEXTURE_2D, tex_id )
		glBegin( GL_QUADS )
		glTexCoord2f( 0.0, 0.0 )
		glVertex3f( self.limits[0], self.limits[1], 0.0 )
		glTexCoord2f( 1.0, 0.0 )
		glVertex3f( self.limits[2], self.limits[1], 0.0 )
		glTexCoord2f( 1.0, 1.0 )
		glVertex3f( self.limits[2], self.limits[3], 0.0 )
		glTexCoord2f( 0.0, 1.0 )
		glVertex3f( self.limits[0], self.limits[3], 0.0 )
		# wall segments
		for w in self.walls:
			# change texture
			if w.texture != tex_id:
				glEnd()
				glBindTexture( GL_TEXTURE_2D, w.texture )
				glBegin( GL_QUADS )
			# wall segment quad
			glTexCoord2f( 0.0, 0.0 )
			glVertex3f( w.vec_from[0], w.vec_from[1], 0.0 )
			glTexCoord2f( 1.0, 0.0 )
			glVertex3f( w.vec_to[0], w.vec_to[1], 0.0 )
			glTexCoord2f( 1.0, 1.0 )
			glVertex3f( w.vec_to[0], w.vec_to[1], w.height )
			glTexCoord2f( 0.0, 1.0 )
			glVertex3f( w.vec_from[0], w.vec_from[1], w.height )
		glEnd()
		# skybox
		glBindTexture( GL_TEXTURE_2D, self.textures.skybox )
		glBegin( GL_QUADS )
		glTexCoord2f( 0.0, 0.0 )
		glVertex3f( -300.0, -300.0, 300.0 )
		glTexCoord2f( 0.33, 0.0 )
		glVertex3f(  300.0, -300.0, 300.0 )
		glTexCoord2f( 0.33, 0.33 )
		glVertex3f(  300.0, -300.0, 0.0 )
		glTexCoord2f( 0.0, 0.33 )
		glVertex3f( -300.0, -300.0, 0.0 )
		glTexCoord2f( 0.33, 0.0 )
		glVertex3f( -300.0, 300.0, 0.0 )
		glTexCoord2f( 0.66, 0.0 )
		glVertex3f(  300.0, 300.0, 0.0 )
		glTexCoord2f( 0.66, 0.33 )
		glVertex3f(  300.0, 300.0, 300.0 )
		glTexCoord2f( 0.33, 0.33 )
		glVertex3f( -300.0, 300.0, 300.0 )
		glTexCoord2f( 0.33, 0.33 )
		glVertex3f( -300.0, -300.0, 0.0 )
		glTexCoord2f( 0.66, 0.33 )
		glVertex3f( -300.0,  300.0, 0.0 )
		glTexCoord2f( 0.66, 0.66 )
		glVertex3f( -300.0,  300.0, 300.0 )
		glTexCoord2f( 0.33, 0.66 )
		glVertex3f( -300.0, -300.0, 300.0 )
		glTexCoord2f( 0.0, 0.33 )
		glVertex3f( 300.0, -300.0, 300.0 )
		glTexCoord2f( 0.33, 0.33 )
		glVertex3f( 300.0,  300.0, 300.0 )
		glTexCoord2f( 0.33, 0.66 )
		glVertex3f( 300.0,  300.0, 0.0 )
		glTexCoord2f( 0.0, 0.66 )
		glVertex3f( 300.0, -300.0, 0.0 )
		glTexCoord2f( 0.66, 0.5 ) # ceiling
		glVertex3f( -300.0, -300.0, 300.0 )
		glTexCoord2f( 1.0, 0.5 )
		glVertex3f( -300.0, 300.0, 300.0 )
		glTexCoord2f( 1.0, 1.0 )
		glVertex3f( 300.0, 300.0, 300.0 )
		glTexCoord2f( 0.66, 1.0 )
		glVertex3f( 300.0, -300.0, 300.0 )
		glEnd()
		# finish display list
		glEndList()

	def __draw__(self):
		# reset frame
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT )
		glBindTexture( GL_TEXTURE_2D, 0 )
		# agent direction in degrees
		dir_n  = self.nav_state[1]
		dir_n /= np.linalg.norm(dir_n)
		dir_a  = asin(abs(dir_n[1]))*RAD2DEG
		if   dir_n[0]<=0 and dir_n[1]>=0: dir_a =180.0-dir_a
		elif dir_n[0]<=0 and dir_n[1]<=0: dir_a =180.0+dir_a
		elif dir_n[0]>=0 and dir_n[1]<=0: dir_a =360.0-dir_a
		# draw world
		x = 0
		w = self.opengl_param.window_width
		h = self.opengl_param.window_height
		for i in range(int(dir_a-w/2),int(dir_a+w/2)+1):
			# setup & render frame slice
			glViewport( x, 0, 1, h )
			glMatrixMode( GL_PROJECTION )
			glLoadIdentity()
			gluPerspective( float(h), 1.0/h, self.opengl_param.clip_near, self.opengl_param.clip_far )
			glMatrixMode( GL_MODELVIEW )
			glLoadIdentity()
			focus = np.array( [self.nav_state[0][0]+cos(i*DEG2RAD)*100.0,
				               self.nav_state[0][1]+sin(i*DEG2RAD)*100.0,
				               self.opengl_param.cam_height] )
			gluLookAt( self.nav_state[0][0], self.nav_state[0][1], self.opengl_param.cam_height,
			           focus[0], focus[1], focus[2], 0.0, 0.0, 1.0 )
			glCallList( self.display_list )
			# next slice
			x+=1
		# write framebuffer image into class-intern storage
		opengl_buffer = glReadPixels( 0,0,w,h, GL_RGBA, GL_UNSIGNED_BYTE )
		frame_img = img.frombuffer( 'RGBA', (w,h), opengl_buffer, 'raw', 'RGBA', 0, 0 )
		visual = np.array(frame_img)[:,:,:3].reshape((1, 38400))
		self.sfa_response = self.sfa_network.execute( visual )
		# finish frame & display
		glutSwapBuffers()

	def __checkPosition__( self, pos ):
		odd_nodes = False
		for w in self.walls:
			# break if too close to any wall
			if w.proximityAlert(pos): return False
			# run Point in Polygon algorithm (@ http://alienryderflex.com/polygon/)
			if w.vec_from[1]<pos[1] and w.vec_to[1]>=pos[1] or w.vec_to[1]<pos[1] and w.vec_from[1]>=pos[1]:
				if (w.vec_from[0]+(pos[1]-w.vec_from[1])/(w.vec_to[1]-w.vec_from[1])*(w.vec_to[0]-w.vec_from[0]) < pos[0]):
					odd_nodes = not odd_nodes
		return odd_nodes

	def __gaussianWhiteNoise2D__( self, ctr_dir=None ):
		# not centered / unrestricted choice
		if ctr_dir == None:
		    a = (np.random.random()*360.0) * DEG2RAD
		    return np.array( [np.cos(a),np.sin(a)] )
		# choice centerec around given velocity vector
		else:
		    try:
		        dir_n = ctr_dir / np.sqrt( ctr_dir[0]**2+ctr_dir[1]**2 )
		        dir_a = np.arcsin( abs(dir_n[1]) ) * RAD2DEG
		        if   dir_n[0]<=0 and dir_n[1]>=0: dir_a =180.0-dir_a
		        elif dir_n[0]<=0 and dir_n[1]<=0: dir_a =180.0+dir_a
		        elif dir_n[0]>=0 and dir_n[1]<=0: dir_a =360.0-dir_a
		        a = (dir_a-320.0/2.0 + rnd()*320.0) * DEG2RAD              
		    except ValueError:
		        return self.__gaussianWhiteNoise2D__() # random rebound in case the path gets stuck in a corner
		return np.array( [np.cos(a),np.sin(a)] )

	#--------------------------------------------------------------[ interface ]

	def randomStep(self):
		mom = 0.55
		while True:
			rnd_dir = self.__gaussianWhiteNoise2D__( self.nav_state[1] )
			step    = self.nav_state[1]*mom + rnd_dir*(1.0-mom)
			step   /= np.linalg.norm(step)
			pos_tmp = self.nav_state[0] + step
			dir_tmp = (pos_tmp-self.nav_state[0])
			mom    *= 0.5
			if self.__checkPosition__( pos_tmp ):
				self.nav_state[0] = pos_tmp
				self.nav_state[1] = dir_tmp
				break
		return self.nav_state

	def getVisualActivity(self):
		glutMainLoopEvent()
		return self.sfa_response

	def getRawSampleAtPosition( self, pos, dir ):
		self.nav_state[ 0 ] = pos
		self.nav_state[ 1 ] = dir
		if self.__checkPosition__( pos ) == False:
			return np.array( [ np.empty( 32 ) * np.NAN ] )
		else:
			return self.getVisualActivity()

	def getGridActivity(self, pos=None):
			return self.grid_cells.sample( self.nav_state[0] if pos == None else pos )

	def getNavState(self):
		return self.nav_state

	def generateNavData( self, steps=1, start_pos=(0.0,0.0) ):
		# prep data
		dat_vis = np.zeros( [steps, 32] )
		dat_grd = np.zeros( [steps, 32] )
		dat_pos = np.zeros( [steps,  2] )
		dat_dir = np.zeros( [steps,  2] )
		# generate data
		for i in range(steps):
			self.randomStep()
			dat_vis[i] = self.getVisualActivity()
			dat_grd[i] = self.getGridActivity()
			dat_pos[i] = self.nav_state[0]
			dat_dir[i] = self.nav_state[1]
			sys.stdout.write('\rGenerating nav data batch ['+str(i+1)+'/'+str(steps)+']')
			sys.stdout.flush()
		# store data
		np.savetxt('./data/dat_vis (raw).txt', dat_vis)
		np.savetxt('./data/dat_grd.txt', dat_grd)
		np.savetxt('./data/dat_pos.txt', dat_pos)
		np.savetxt('./data/dat_dir.txt', dat_dir)
		# post process sfa data

		""" new post-processing (scale and invert)
		for i in range( 32 ):
			dat_vis[:,i] /= np.max( np.abs(dat_vis[:,i]) )  # (1) scale down to unit range
			if np.abs( np.min(dat_vis[:,i]) ) > \
			   np.abs( np.max(dat_vis[:,i]) ):              # (2) polarization heuristic:     
				dat_vis[:,i] *= -1.0                        # largest abs value should be pos
		"""

		#""" previous post-processing
		for i in range(32):
			dat_vis[:,i]  = abs(dat_vis[:,i])              # flip negative values up; alt. push all values up: -= np.nanmin(dat_vis[:,i])
			dat_vis[:,i] *= (1.0/np.nanmax(dat_vis[:,i]))  # scale each signal to [0;1]
		#"""

		np.savetxt('./data/dat_vis.txt', dat_vis)
		# report
		print 'Full nav data batch generated successfully. Added files:'
		print 'dat_vis (raw).txt, dat_vis.txt, dat_grd.txt, dat_pos.txt, dat_dir.txt'
