
#=====================================================================[ header ]

import os
import sys
import itertools
import cPickle as pickle

import numpy
import mdp

import matplotlib                     
matplotlib.use('Agg')  # non-interactive 'Agg' (.png) backend
import matplotlib.pyplot as plt

from freezeable  import *
from virtualMaze import *

#==============================================================[ sampler class ]

class Sampler( Freezeable ):

	def __init__( self, sample_limits, sample_func, sample_signals=32, sample_dir=['n','e','s','w','ne','se','sw','nw'] ):
		# setup sampler parameters
		self.directions = { 'n' : np.array([ 0.0, 1.0 ]),
		                    'e' : np.array([ 1.0, 0.0 ]),
		                    's' : np.array([ 0.0,-1.0 ]),
		                    'w' : np.array([-1.0, 0.0 ]),
		                    'ne': np.array([ 1.0, 1.0 ]) / np.sqrt(2.0),
		                    'se': np.array([ 1.0,-1.0 ]) / np.sqrt(2.0),
		                    'sw': np.array([-1.0,-1.0 ]) / np.sqrt(2.0),
		                    'nw': np.array([-1.0, 1.0 ]) / np.sqrt(2.0) }
		self.sample_dir = np.array( [ self.directions[d] for d in sample_dir ] )
		self.sample_lim = sample_limits   # sampling limits
		self.sample_fnc = sample_func     # function to be sampled over given limits (prototype: def f(pos, dir))
		self.sample_sig = sample_signals  # sample_func output dim
		self.directions = sample_dir      # sampling directories as characters (for directories)
		# setup sampler directories for plots & data
		self.__setupDirectories__()
		# freeze class members
		self.freeze()

	def __setupDirectories__( self ):
		# list of sample & data directories
		dir_list  = [ './sampler', './sampler/plots', './sampler/data' ]
		dir_list += [ dir_list[1] + '/' + d for d in self.directions ]
		dir_list += [ dir_list[2] + '/' + d for d in self.directions ]
		if len( self.sample_dir ) > 1: 
			dir_list += [ dir_list[1] + '/avg', dir_list[2] + '/avg' ] 
		# create directories as necessary
		for d in dir_list:
			success = True
			if os.path.isdir( d ) == False:
				try:    os.mkdir( d )
				except: success = False
		return success

	def __plot__( self, sample_pos, sample_data, sig_id, value_max, data_dir ):
		filename = 'sig_' + str(sig_id).zfill(3) + ' [' + data_dir + '] (' + str(np.nanmin(sample_data)) + ',' + str(np.nanmax(sample_data)) + ')'
		# plot heat maps via hexbin
		plt.clf()
		plt.figure().set_size_inches( 3, 3 )
		plt.hexbin( sample_pos[:,0], sample_pos[:,1], C=sample_data.reshape( len(sample_pos) ), vmax=value_max )
		plt.gca().invert_yaxis()
		cb = plt.colorbar()
		plt.savefig(  'sampler/plots/' + data_dir + '/' + filename + '.png', dpi=300 )
		plt.close()
		# store the raw numbers as well
		numpy.savetxt( 'sampler/data/' + data_dir + '/' + filename + '.txt', sample_data )

	def execute( self, scale='local', sample_rate=1 ):

		# setup
		data = np.zeros( [ len(self.sample_dir),                                   # no. of directions
	                       (self.sample_lim[2] - self.sample_lim[0])/sample_rate,  # x-axis pos range
 	                       (self.sample_lim[3] - self.sample_lim[1])/sample_rate,  # y-axis pos range
	                       self.sample_sig ] )                                     # no. of signals
		
		i = np.arange( self.sample_lim[0], self.sample_lim[2], sample_rate, dtype=np.int32 )
		j = np.arange( self.sample_lim[1], self.sample_lim[3], sample_rate, dtype=np.int32 )
		sample_pos = np.array( [ p for p in itertools.product( i, j ) ] )
		
		index_pos = np.indices( [ data.shape[1], data.shape[2] ] ) \
		            .reshape( 2, data.shape[1]*data.shape[2] )     \
		            .transpose() # data at sample_pos[k] accessible via data[ .., index_pos[k], .. ]
		
		# sample
		for i_d, d in enumerate( self.sample_dir ):
			for i_p, p in enumerate( sample_pos ):
				data[ i_d, index_pos[i_p][0], index_pos[i_p][1] ] = self.sample_fnc( pos=p, dir=d )
				# show progress
				done = int( 50.0 * (i_p+1) / len(sample_pos) )
				sys.stdout.write( '\r' + '[SAMPLING][' + '='*done + '-'*(50-done) + ']~[' + '%d/%.2f' % ( i_p+1, 100.0*(i_p+1)/len(sample_pos) ) + '%]' )
				sys.stdout.flush()
			print ''

		nan_mask = np.isnan( data )
		data[nan_mask] = 0.0

		for i in range( len(data) ):
			data[i] = np.exp(data[i]) / np.sum(np.exp(data[i]))

		data[nan_mask] = np.NAN

		# plot
		if scale == 'global': scale = np.nanmax( np.abs( data ) )
		# plot :: individual directions
		for i_d, d in enumerate( self.sample_dir ):
			for i in range( self.sample_sig ):
				self.__plot__( sample_pos, 
					           data[ i_d, :, :, i ], i+1,
					           np.nanmax( data[:,:,:,i] ) if scale == 'local' else scale,
					           self.directions[i_d] )
		
		if len( self.sample_dir ) > 1:
			data = data.mean( 0 )
			for i in range( self.sample_sig ):
				self.__plot__( sample_pos, 
					           data[ :, :, i ], i+1, 
					           np.nanmax( data[:,:,i] ) if scale == 'local' else scale,
					           'avg' )
