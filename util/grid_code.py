
#=====================================================================[ Header ]

import numpy as np
from PIL import Image     as IMG
from PIL import ImageDraw as IMG_DRW

from colorscale import colorScale
from freezeable import *

RAD2DEG = 180.0/np.pi
DEG2RAD = np.pi/180.0


#============================================================[ Artificial Grid ]

class ArtificialGrid( Freezeable ):
	"""
	Class for a single artificial (i.e., not based on a biological mechanism) grid cell.
	"""

	def __init__( self, grid_spacing=1.0, grid_offset=np.array([0,0]), grid_rotation=0.0 ):
		"""
		<spacing>       : grid size.
		<grid_offset>   : spatial offset from (0,0).
		<grid_rotation> : rotation of the overall grid.
		"""
		# grid parameter values
		self.offset   = grid_offset
		self.spacing  = grid_spacing
		self.rotation = grid_rotation
		# grid main axes
		self.__axis_a__  = np.array([np.cos((0+grid_rotation)*DEG2RAD),np.sin((0+grid_rotation)*DEG2RAD)])
		self.__axis_a__ /= np.sqrt(np.vdot(self.__axis_a__,self.__axis_a__))
		self.__axis_b__  = np.array([np.cos((120+grid_rotation)*DEG2RAD),np.sin((120+grid_rotation)*DEG2RAD)])
		self.__axis_b__ /= np.sqrt(np.vdot(self.__axis_b__,self.__axis_b__))
		self.__axis_c__  = np.array([np.cos((240+grid_rotation)*DEG2RAD),np.sin((240+grid_rotation)*DEG2RAD)])
		self.__axis_c__ /= np.sqrt(np.vdot(self.__axis_c__,self.__axis_c__))
		# freeze class
		self.freeze()

	def sample( self, pos ):
		val = np.cos( np.vdot(self.__axis_a__,pos-self.offset)*1/self.spacing ) + \
			  np.cos( np.vdot(self.__axis_b__,pos-self.offset)*1/self.spacing ) + \
			  np.cos( np.vdot(self.__axis_c__,pos-self.offset)*1/self.spacing )
		return (val+1.5)/4.5 # scale activity range to [0.0,1.0]

#--------------------------------------------------[ Artificial Grid Population ]

class ArtificialGridCode( Freezeable ):
	"""
	Class for a population of artificial grid cells.
	"""

	def __init__( self, no_of_grids, grid_spacing_min, grid_spacing_max, rnd_offset_max=0.0, rnd_rotation_max=0.0 ):
		"""
		<no_of_grid>       : number of grid cells forming the population
		<grid_spacing_*>   : limits for smallest and largest grid spacing
		<rnd_offset_max>   : maximum allowed spatial offset for each grid cell
		<rnd_rotation_max> : maximum allowed rotation for each grid cell
		"""
		# grid population
		np.random.seed(42)
		self.grid = []
		for i in range(no_of_grids):
			new_grid = ArtificialGrid( grid_spacing_min+i*(grid_spacing_max-grid_spacing_min)/no_of_grids,
									   np.random.random([2])*rnd_offset_max, 
									   np.random.random()*rnd_rotation_max )
			self.grid.append( new_grid )
		# freeze
		self.freeze()

	def sample( self, pos ):
		population_activity = np.zeros( len(self.grid) )
		for i in range(len(self.grid)):
			population_activity[i] = self.grid[i].sample( pos )
		return population_activity # activity range [0.0,1.0]

	def plot(self):
		# sample
		dim  = (80,80)
		data = np.zeros([len(self.grid),dim[0],dim[1]])
		for x in range(dim[0]):
			for y in range(dim[1]):
				data[:,x,y] = self.sample((x,y))
		# plot
		img_scale = 4
		for g in range(len(self.grid)):
			img = IMG.new( 'RGB', dim, 'white' )
			pix = img.load()
			for x in range(dim[0]):
				for y in range(dim[1]):
					pix[x,y] = colorScale( data[g,x,y], 0.0, 1.0 )
			img = img.resize( (dim[0]*img_scale,dim[1]*img_scale) )
			img = img.transpose( IMG.FLIP_TOP_BOTTOM )
			img.save( 'grid_'+str(g)+'.png' )

#---------------------------------------------------------------------[ Testing ]

def runGridTest():
	grid = ArtificialGridCode( no_of_grids     = 8, 
		                       grid_spacing_min= 5.0, 
		                       grid_spacing_max= 20.0,
		                       rnd_offset_max  = 25.0 )
	grid.plot()
